//
//  HomeScreenNib.swift
//  MySchool
//
//  Created by laughinggg on 12/9/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit

class HomeScreenNib:UITableViewCell{
    
    @IBOutlet weak var img:UIImageView!
    @IBOutlet weak var lbltitle:UILabel!
    @IBOutlet weak var lblSchoolType:UILabel!
    @IBOutlet weak var txtfDescrip:UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
