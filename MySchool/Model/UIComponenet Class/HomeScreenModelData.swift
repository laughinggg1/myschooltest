//
//  HomeScreenModelData.swift
//  MySchool
//
//  Created by laughinggg on 12/11/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import Foundation

struct schoolStruct:Decodable {
    let id : String
    let name: String
    let description: String
    let logo: String
    let type: String
    let rate:String
    let lat:String
    let lng:String
}
