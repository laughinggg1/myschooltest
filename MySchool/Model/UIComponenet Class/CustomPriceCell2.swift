//
//  CustomPriceCell2.swift
//  MySchool
//
//  Created by laughinggg on 10/24/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit

class CustomPriceCell2: UITableViewCell {
    
    
    @IBOutlet weak var lblGrade2: UILabel!
    @IBOutlet weak var lblPrice2Plan1: UILabel!
    @IBOutlet weak var lblPrice2Plan2: UILabel!
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblGrade2?.text = "hi"
        lblPrice2Plan1?.text = "hi"
        lblPrice2Plan2?.text = "hi"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
