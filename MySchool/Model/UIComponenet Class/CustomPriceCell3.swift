//
//  CustomPriceCell.swift
//  MySchool
//
//  Created by laughinggg on 10/23/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit

class CustomPriceCell3: UITableViewCell {
    
    @IBOutlet weak var lblGrade3: UILabel!
    @IBOutlet weak var lblPrice3Plan1: UILabel!
    @IBOutlet weak var lblPrice3Plan2: UILabel!
    @IBOutlet weak var lblPrice3Plan3: UILabel!
    @IBOutlet weak var lblPrice3Plan4: UILabel!
   
    
    @IBOutlet weak var stkPriceBundle: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code 
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
