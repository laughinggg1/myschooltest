//
//  JsonUrl.swift
//  MySchool
//
//  Created by laughinggg on 12/8/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import Foundation
import UIKit
var schooltype:Int = 0
//var schoolStructs = [schoolStruct]()

class JsonUrl {
    
    static func dataUrl(for type: Int) -> String {
        return "http://178.128.50.11/myschool/api/school.php?type=\(type)&fbclid=IwAR09r9VkF0GyHzy-GcwHL9hFNJaazcIOULeSV-hfxYmuuRSiognno7a8ZMA"
    }
    
    static func logoUrl(imgName: String) -> String{
        return "http://178.128.50.11/myschool/images/\(imgName)"
    }
    static func getSchool(schoolid: String) -> String{
        return "http://178.128.50.11/myschool/api/school.php?getSchool=\(schoolid)"
    }
    static func getPricePlan(schoolid: String, priceinfo: Int) -> String {
        return "http://178.128.50.11/myschool/api/school.php?getPriceInfo\(priceinfo)=\(schoolid)"
    }
    
    
}

struct alertController {
    static func showAlertMessage(title: String, message: String, inViewController vc: UIViewController){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        vc.present(alertController, animated: true, completion: nil)
    }
}
