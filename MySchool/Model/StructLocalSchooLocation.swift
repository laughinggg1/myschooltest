//
//  StructLocalSchooLocation.swift
//  MySchool
//
//  Created by laughinggg on 11/27/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import Foundation

struct SchoolLocation {
    var schoolname: String
    var latitute: Double
    var longtitute: Double
}
var SchoolLocationList = [SchoolLocation(schoolname: "AIS Maosetoung", latitute: 11.547172, longtitute: 104.908528), SchoolLocation(schoolname: "AIS TK", latitute: 11.581807, longtitute: 104.904451),SchoolLocation(schoolname: "AIS CA", latitute: 11.492342, longtitute: 104.944472),SchoolLocation(schoolname: "IFL", latitute: 11.569535, longtitute: 104.893649),SchoolLocation(schoolname: "RUPP", latitute: 11.568351, longtitute: 104.890883),SchoolLocation(schoolname: "Mekong University", latitute: 11.557719, longtitute: 104.887452),SchoolLocation(schoolname: "UC", latitute: 11.558839, longtitute: 104.871411),SchoolLocation(schoolname: "Beltie University Campus 1", latitute: 11.548188, longtitute: 104.917118),SchoolLocation(schoolname: "Beltie University campus 2 comming soon", latitute: 11.524980, longtitute: 104.824050)]
