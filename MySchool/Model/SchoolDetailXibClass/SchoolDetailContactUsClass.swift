//
//  SchoolDetailContactUs.swift
//  MySchool
//
//  Created by laughinggg on 11/25/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit

class SchoolDetailContactUsClass:UITableViewCell{
    @IBOutlet weak var lblBrandName: UILabel!
    @IBOutlet weak var txtviewAddress:UITextView!
    @IBOutlet weak var txtviewTelephone: UITextView!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblWebsite: UILabel!
}
