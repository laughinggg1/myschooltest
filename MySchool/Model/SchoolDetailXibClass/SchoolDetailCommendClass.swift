//
//  SchoolDetailCommend.swift
//  MySchool
//
//  Created by laughinggg on 11/25/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit

class SchoolDetailCommendClass:UITableViewCell{
    @IBOutlet weak var imgProfile:UIImageView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var viewStar:UIView!
    @IBOutlet weak var lblTimeAgo:UILabel!
    @IBOutlet weak var txtviewCommend:UITextView!
}
