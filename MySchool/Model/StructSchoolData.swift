//
//  LocalData.swift
//  MySchool
//
//  Created by laughinggg on 11/9/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import Foundation

struct gradeArrayData {
    var id:Int?
    var grade: String?
    
}
struct PricePlan1Data {
    
    var priceGrade1:String?
    
    
}
struct PricePlan2Data {
    
    var priceGrade1:String?
    var priceGrade2:String?
}
struct PricePlan3Data {
    
    var priceGrade1:String?
    var priceGrade2:String?
    var priceGrade3:String?
    var priceGrade4:String?
}
struct schoolnameData {
    var schoolname:String?
    var schoolID:String?
}
var schoolnameArray = [ schoolnameData(schoolname: "សាលារៀនអន្តរទ្វីប អាមេរិកាំង", schoolID: "S1"), schoolnameData(schoolname: "វិទ្យាស្ថានអន្តរទ្វីបអាមេរិកាំង", schoolID:"S2" )
    ,]

var gradeArry = [
    gradeArrayData (id: 1, grade: "មត្តេយ្យសិក្សា"),
    gradeArrayData (id: 2, grade: "ថ្នាក់ទី១"),
    gradeArrayData (id: 3, grade: "ថ្នាក់ទី២"),
    gradeArrayData (id: 4, grade: "ថ្នាក់ទី៣​ ដល់​ ថ្នាក់ទី៦"),
    gradeArrayData (id: 5, grade: "ថ្នាក់ទី៧ ដល់ ថ្នាក់ទី៩"),
    gradeArrayData (id: 6, grade: "ថ្នាក់ទី៩ ដល់ ថ្នាក់ទិ១២")
    ,]
var pricePlan1 = [PricePlan1Data(priceGrade1: "1,013 USD"),
                  PricePlan1Data(priceGrade1:"1, 013 USD"),
                  PricePlan1Data(priceGrade1:"1, 390 USD"),
                  PricePlan1Data(priceGrade1:"1, 490 USD"),
                  PricePlan1Data(priceGrade1:"1, 650 USD"),
                  PricePlan1Data(priceGrade1:"1, 750 USD")
    
    ,]
var pricePlan2 = [ PricePlan2Data(priceGrade1: "522 USD", priceGrade2: "522 USD"),
                   PricePlan2Data(priceGrade1: "522 USD", priceGrade2: "522 USD"),
                   PricePlan2Data(priceGrade1: "716 USD", priceGrade2: "716 USD"),
                   PricePlan2Data(priceGrade1: "767 USD", priceGrade2: "767 USD"),
                   PricePlan2Data(priceGrade1: "850 USD", priceGrade2: "850 USD"),
                   PricePlan2Data(priceGrade1: "901 USD", priceGrade2: "901 USD")
    ,]
var pricePlan3 = [
    PricePlan3Data(priceGrade1: "266 USD", priceGrade2: "266 USD", priceGrade3: "266 USD", priceGrade4: "266 USD"),
    PricePlan3Data(priceGrade1: "266 USD", priceGrade2: "266 USD", priceGrade3: "266 USD", priceGrade4: "266 USD"),
    PricePlan3Data(priceGrade1: "365 USD", priceGrade2: "365 USD", priceGrade3: "365 USD", priceGrade4: "365 USD"),
    PricePlan3Data(priceGrade1: "391 USD", priceGrade2: "391 USD", priceGrade3: "391 USD", priceGrade4: "391 USD"),
    PricePlan3Data(priceGrade1: "433 USD", priceGrade2: "433 USD", priceGrade3: "433 USD", priceGrade4: "433 USD"),
    PricePlan3Data(priceGrade1: "459 USD", priceGrade2: "459 USD", priceGrade3: "459 USD", priceGrade4: "459 USD")
    ,]


