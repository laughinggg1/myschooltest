//
//  CustomHomeVC.swift
//  MySchool
//
//  Created by laughinggg on 11/18/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import GoogleMaps
    
class HomeScreenVC: ButtonBarPagerTabStripViewController {
    @IBOutlet weak var viewBackGround: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        navigationController?.navigationBar.backgroundColor = UIColor.middlePink
//        navigationController?.navigationBar.backgroundColor = UIColor.white
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //navigationController?.navigationBar.backgroundColor = UIColor.middlePink
    }
    
    var schoolStructs = [schoolStruct]()
    override func viewDidLoad() {
        ButtonBarSetting()
        super.viewDidLoad()
        viewBackGround.backgroundColor = UIColor.darkPurple
        self.navigationItem.title = "MySchool"
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let kindergartenvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "kindergartenvc")
        let primaryschoolvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "primaryschoolvc")
        let highschoolvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "highschoolvc")
        let universityvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "universityvc")
        return [kindergartenvc, primaryschoolvc, highschoolvc, universityvc]
    }
    
    func ButtonBarSetting() {
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.buttonBarItemFont = UIFont(name: "Helvetica", size: 16.0)!
        settings.style.buttonBarItemTitleColor = .gray
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 15
        settings.style.buttonBarRightContentInset = 15
        settings.style.selectedBarHeight = 3.0
        settings.style.selectedBarBackgroundColor = .gray
        settings.style.buttonBarHeight = 20.0
        // Changing item text color on swipe
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .gray
            newCell?.label.textColor = UIColor(red: 189/255, green: 16/255, blue: 224/255, alpha: 1)
        
        }
    }

}
