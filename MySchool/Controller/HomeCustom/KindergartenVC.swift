//
//  KindergartenVC.swift
//  MySchool
//
//  Created by laughinggg on 11/18/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SDWebImage


class KindergartenVC: UITableViewController,IndicatorInfoProvider {
    
    @IBOutlet weak var refreshingControl: UIRefreshControl!
    var schoolStructs = [schoolStruct]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTable()
        getSchoolData()
        
        
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "មតេ្តយ្យសិក្សា")
    }
    func registerTable(){
        let nibhome = UINib(nibName: "CustomHomeViewCell", bundle: nil)
        tableView.register(nibhome, forCellReuseIdentifier: "homescreencell")
        tableView.addSubview(refreshingControl)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolStructs.count
    }
    
    func getSchoolData(){
        guard let jsonurl = URL(string: JsonUrl.dataUrl(for: 2)) else {return}
        URLSession.shared.dataTask(with: jsonurl) { (data, response, err) in
            guard let data = data else {return}
            let jsondecode = JSONDecoder()
            do {
                
                let school = try jsondecode.decode([schoolStruct].self, from: data)
                self.schoolStructs = school
                
            } catch {
                Utility.showAlertMessage(title: "Cannot get the data from server", message: "Request time out, try again later.", inviewcontroller: self)
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData() 
            }
        }.resume()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homescreencell", for: indexPath) as! HomeScreenNib
        let arrayschool = schoolStructs[indexPath.row]
//        let backgroundcolor = UIView()
//        backgroundcolor.backgroundColor = UIColor.white
//        cell.selectedBackgroundView = backgroundcolor
        cell.lbltitle?.text = arrayschool.name
        cell.lblSchoolType?.text = arrayschool.type
        cell.txtfDescrip?.text = arrayschool.description
        
//        for let url = URL(string: "http://178.128.50.11/myschool/images/\(arrayschool.logo)") {
//        URLSession.shared.dataTask(with: url) { (data, response, err) in
//            guard let data = data else {return}
//            DispatchQueue.main.async {
//                cell.img.image = UIImage(data: data)
//                self.tableView.reloadData()
//            }
//        }.resume()
//     }
        let imgUrl = JsonUrl.logoUrl(imgName: arrayschool.logo)
        if let schoolURL = URL(string: imgUrl) {
            cell.img.sd_setImage(with: schoolURL, placeholderImage:UIImage(named: "Placeholder.png"))
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "schooldetail") as! SchoolDetailVC
        navigationController?.present(destination, animated: true, completion: nil)
    }
    @IBAction func onRefreshing(_ sender: Any){
        print("1")
        
    }
}
