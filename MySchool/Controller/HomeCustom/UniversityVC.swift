//
//  UniversityVC.swift
//  MySchool
//
//  Created by laughinggg on 11/18/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SDWebImage

class UniversityVC: UITableViewController,IndicatorInfoProvider {
    
    var schoolDatas = [schoolStruct]()
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTable()
        getSchoolData()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "សកលវិទ្យាល័យ")
    }
    
    func registerTable(){
        let nibhome = UINib(nibName: "CustomHomeViewCell", bundle: nil)
        tableView.register(nibhome, forCellReuseIdentifier: "homescreencell")
    }
    
    func getSchoolData(){
        guard let schoolUrl = URL(string: JsonUrl.dataUrl(for: 4)) else {return}
        URLSession.shared.dataTask(with: schoolUrl) { (data, response, err) in
            guard let data = data else {return}
            do{
                let jsondecode = JSONDecoder()
                let school = try jsondecode.decode([schoolStruct].self, from: data)
                self.schoolDatas = school
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }catch {
                Utility.showAlertMessage(title: "Unable to show the data right now", message: "Request time out, try again later.", inviewcontroller: self)
            }
        }.resume()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolDatas.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homescreencell", for: indexPath) as! HomeScreenNib
        let schoolarray = schoolDatas[indexPath.row]
        cell.lbltitle?.text = schoolarray.name
        cell.lblSchoolType?.text = schoolarray.type
        cell.txtfDescrip?.text = schoolarray.description
        if let logo = URL(string: JsonUrl.logoUrl(imgName: schoolarray.logo)){
            cell.img?.sd_setImage(with: logo, placeholderImage: UIImage(named: "Placeholder.png"))
        }

        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "schooldetail") as! SchoolDetailVC
        navigationController?.present(destination, animated: true, completion: nil)
    }
    
    
}
