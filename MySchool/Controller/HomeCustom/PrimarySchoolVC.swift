//
//  PrimarySchoolVC.swift
//  MySchool
//
//  Created by laughinggg on 11/18/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SDWebImage

class PrimarySchoolVC: UITableViewController,IndicatorInfoProvider {
    
    var schoolStructs = [schoolStruct]()
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTable()
        getSchoolData()
        
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "បឋមសិក្សា")
    }
    
    func registerTable(){
        let nibfile = UINib(nibName: "CustomHomeViewCell", bundle: nil)
        tableView.register(nibfile, forCellReuseIdentifier: "homescreencell")
    }
    
    func getSchoolData(){
        guard let schoolUrl = URL(string: JsonUrl.dataUrl(for: 1)) else {return}
        URLSession.shared.dataTask(with: schoolUrl) { (data, response, err) in
            guard let data = data else {return}
            let jsondecoder = JSONDecoder()
            do {
                let schools = try jsondecoder.decode([schoolStruct].self, from: data)
                self.schoolStructs = schools
            }catch{
                
                 Utility.showAlertMessage(title: "Cannot get the data from server", message: "Request time out, try again later.", inviewcontroller: self)
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }.resume()
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolStructs.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homescreencell", for: indexPath) as! HomeScreenNib
        let schoolarray = schoolStructs[indexPath.row]
        cell.lbltitle?.text = schoolarray.name
        cell.lblSchoolType?.text = schoolarray.type
        cell.txtfDescrip?.text = schoolarray.description
        if let logoUrl = URL(string: JsonUrl.logoUrl(imgName: schoolarray.logo)) {
        cell.img.sd_setImage(with: logoUrl, placeholderImage: UIImage(named: "Placeholder.png"))
        }
        
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //performSegue(withIdentifier: "goToDetail", sender: nil)
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "schooldetail") as! SchoolDetailVC
        navigationController?.present(destination, animated: true, completion: nil)
        
    }
}
