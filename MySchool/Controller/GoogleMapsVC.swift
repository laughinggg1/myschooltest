//
//  GoogleMapsVC.swift
//  MySchool
//
//  Created by laughinggg on 11/26/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit
import GoogleMaps

class GoogleMapsVC: UIViewController, GMSMapViewDelegate,CLLocationManagerDelegate {
    @IBOutlet weak var mapView: GMSMapView!
    
    let locationManager = CLLocationManager()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //navigationController?.navigationBar.isHidden = true
        navigationController?.setNavigationBarHidden(true, animated: animated)
        navigationController?.navigationBar.isTranslucent = false
        
        //navigationController?.navigationBar.backgroundColor = .clear
        //NavigationTransparaent()
    }
    
    override func viewDidLoad() {
        mapView.delegate = self
        super.viewDidLoad()
        loadMapSetting()
        cameraPosition()
        schoolLocationList()
        
        locationManager.requestAlwaysAuthorization()
        navigationController?.navigationItem.title = "School Location"
        
        requestLocationUpdate()
        
        let lastknownlocation = locationManager.location
        
        if let lastknownlocation = lastknownlocation {
            // Move camera to last known location
            let lastKnownLocationCameraPosition = GMSCameraPosition(target: lastknownlocation.coordinate, zoom: 14, bearing: 0, viewingAngle: 0)
            mapView.animate(to: lastKnownLocationCameraPosition)
            
            // Add marker to last known location
            let marker = GMSMarker(position: lastknownlocation.coordinate)
            marker.title = "You are here!"
            marker.map = mapView
        }
        //        guard let lastknownlocation = lastknownlocation else {
        //            let lastKnownLocationCameraPosition = GMSCameraPosition(target: lastknownlocation.coordinate, zoom: 14, bearing: 0, viewingAngle: 0)
        //                        mapView.animate(to: lastKnownLocationCameraPosition)
        //
        //                        // Add marker to last known location
        //                        let marker = GMSMarker(position: lastknownlocation.coordinate)
        //                        marker.title = "You are here!"
        //                        marker.map = mapView
        //        }
        //        if lastknownlocation == nil {
        //            print("Cant get last known location.")
        //        } else {
        //            // Move camera to last known location
        //            let lastKnownLocationCameraPosition = GMSCameraPosition(target: lastknownlocation!.coordinate, zoom: 14, bearing: 0, viewingAngle: 0)
        //            mapView.animate(to: lastKnownLocationCameraPosition)
        //
        //            // Add marker to last known location
        //            let marker = GMSMarker(position: lastknownlocation.coordinate)
        //            marker.title = "You are here!"
        //            marker.map = mapView
        //        }
        
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
    }
    
    func loadMapSetting() {
        
        mapView.isUserInteractionEnabled = true
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
    }
    
    func schoolLocationList() {
        for SchoolLocation in SchoolLocationList {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2DMake(SchoolLocation.latitute, SchoolLocation.longtitute)
            marker.title = SchoolLocation.schoolname
            marker.map = mapView
        }
    }
    
    func cameraPosition() {
        let camera = GMSCameraPosition.camera(withLatitude: 11.569129, longitude: 104.8863263, zoom: 16.0)
        mapView.animate(to: camera)
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        if let lastKnownlocation = locationManager.location?.coordinate {
            moveCamera(location: lastKnownlocation)
            print(123)
        }
        return true
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied {
            Utility.showAlertMessage(title: "Location Privacy", message: "The app cant work properly because you did not allow location privacy. Please go to Settings to allow.", inviewcontroller: self)
            return
        } else if status == .authorizedWhenInUse {
            print("Authorized when in use")
        } else{
            print("User allows always.")
        }
        
    }
    private func requestLocationUpdate(){
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 100
        locationManager.startUpdatingLocation()
    }
    func moveCamera(location: CLLocationCoordinate2D) {
        let camera = GMSCameraPosition.camera(withLatitude: location.latitude, longitude: location.longitude, zoom: 14)
        mapView.animate(to: camera)
    }
    
}
