//
//  SettingsVC.swift
//  MySchool
//
//  Created by laughinggg on 11/24/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase
import FirebaseAuth

class NotyetLoginVC: UIViewController, FBSDKLoginButtonDelegate{
    
    @IBOutlet weak var fbLoginButton:FBSDKLoginButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if FBSDKAccessToken.current() != nil {
            self.performSegue(withIdentifier: "alreadylogin", sender: nil)
        }
        
        fbLoginButton.readPermissions = ["email","public_profile"]
        fbLoginButton.delegate = self
        //performSegue(withIdentifier: "logged_in", sender: nil)
        
        navigationController?.navigationItem.title = "Settings"
    }
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
//        if result.isCancelled {
//            print("Button Cancel")
//        }
//        guard error == nil else {
//
//           print(error.localizedDescription)
//            return
//        }
        if result.isCancelled {
            print("Button cancel")
        } else if result.isCancelled {
            return
        } else if result == nil {
            print("Missing access token: \(error.localizedDescription)")
        } else {
            let credential = FacebookAuthProvider.credential(withAccessToken: result.token.tokenString)
            Auth.auth().signInAndRetrieveData(with: credential, completion: {(result,error) in
                self.performSegue(withIdentifier: "alreadylogin", sender: nil)
        })
        }
        
        
//        guard result.token != nil else {
//            print("Missing access token: \(error.localizedDescription)")
//            return
//        }
//        let credential = FacebookAuthProvider.credential(withAccessToken: result.token.tokenString)
//        Auth.auth().signInAndRetrieveData(with: credential, completion: {(result,error) in
//            print("you are logged in")
//
//        })
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        
    }
    
    
    
    
}
