//
//  PricePlan3VC.swift
//  MySchool
//
//  Created by laughinggg on 11/14/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class PricePlan3VC: UITableViewController,IndicatorInfoProvider {
   
    override func viewDidLoad() {
        super.viewDidLoad()
        let Nib3 = UINib(nibName: "CustomPricePlan3", bundle: nil)
        tableView.register(Nib3, forCellReuseIdentifier: "CustomPrice3")
        
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pricePlan3.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomPrice3", for: indexPath) as! CustomPriceCell3
        let gradelist = gradeArry[indexPath.row]
        let pricelist3 = pricePlan3[indexPath.row]
        
        cell.lblGrade3?.text = gradelist.grade
        cell.lblPrice3Plan1?.text = pricelist3.priceGrade1
        cell.lblPrice3Plan2?.text = pricelist3.priceGrade2
        cell.lblPrice3Plan3?.text = pricelist3.priceGrade3
        cell.lblPrice3Plan4?.text = pricelist3.priceGrade4
        return cell
    }
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Plan 3")
    }
    
}
