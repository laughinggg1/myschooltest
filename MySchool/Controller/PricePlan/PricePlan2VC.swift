//
//  PricePlan2VC.swift
//  MySchool
//
//  Created by laughinggg on 11/14/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class PricePlan2VC: UITableViewController, IndicatorInfoProvider {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let Nib2 = UINib(nibName: "CustomPricePlan2", bundle: nil)
        tableView.register(Nib2, forCellReuseIdentifier: "CustomPrice2")
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pricePlan2.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomPrice2", for: indexPath) as! CustomPriceCell2
        let gradelist = gradeArry[indexPath.row]
        let pricelist2 = pricePlan2 [indexPath.row]
        cell.lblGrade2?.text = gradelist.grade
        cell.lblPrice2Plan1?.text = pricelist2.priceGrade1
        cell.lblPrice2Plan2?.text = pricelist2.priceGrade2
        return cell
        
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Plan 2")
    }
    
    
    
}
