//
//  PricePlan1VC.swift
//  MySchool
//
//  Created by laughinggg on 11/14/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class PricePlan1VC: UITableViewController, IndicatorInfoProvider{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let Nib1 = UINib(nibName: "CustomPricePlan1", bundle: nil)
        tableView.register(Nib1, forCellReuseIdentifier: "CustomPrice1")
    }
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Plan 1")
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pricePlan1.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomPrice1", for: indexPath) as! CustomPriceCell1
        let gradelist = gradeArry[indexPath.row]
        let pricelist1 = pricePlan1 [indexPath.row]
        
        
        cell.lblGrade1?.text = gradelist.grade
        cell.lblPrice1Plan1?.text = pricelist1.priceGrade1
    
    return cell
        
    }
    
    
    
}
