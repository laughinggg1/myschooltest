//
//  SchoolDetailContactUsVC.swift
//  MySchool
//
//  Created by laughinggg on 11/25/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class SchoolDetailContactUsVC:
UITableViewController,IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Contact Us")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nibContactUs = UINib(nibName: "SchoolDetailContactUsNib", bundle: nil)
        tableView.register(nibContactUs, forCellReuseIdentifier: "SchContactCell")
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchContactCell", for: indexPath) as! SchoolDetailContactUsClass
        cell.lblBrandName?.text = "Branch Name"
        cell.txtviewAddress?.text = "អគារលេខ១៥៤ ផ្លូវ៤០២ មហាវិថីម៉ៅសេទុង សង្កាត់ទំនប់ទឹក ខណ្ឌចំការមន រាជធានីភ្នំពេញ"
        cell.txtviewTelephone?.text = "(៨៥៥) ២៣ ២២៣ ២៩៥, (៨៥៥) ២៣ ២២១ ២២២, (៨៥៥) ១១ ៣៨៨ ៨៦៨, (៨៥៥) ៩៣ ២១៧ ២១៧"
        cell.lblEmail?.text = "laughinggg001@gmail.com"
        cell.lblWebsite?.text = "www.ais.edu.kh, www.aismtt.edu.kh"
        
        return cell
        
    }
}
