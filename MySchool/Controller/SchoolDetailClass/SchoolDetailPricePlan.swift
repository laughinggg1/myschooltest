//
//  ViewController.swift
//  MySchool
//
//  Created by laughinggg on 10/23/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit
import XLPagerTabStrip
var selectedSegment:Int?

class SchoolDetailPricePlan: UIViewController, UITableViewDataSource,UITableViewDelegate,IndicatorInfoProvider {
   
    @IBOutlet weak var segPlanControl: UISegmentedControl!
    @IBOutlet weak var tblViewHome: UITableView!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //navigationController?.navigationBar.isHidden = true
        loadNIB()
        segSelect(select: 1, index: 0)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = .clear
        
        
    }
    
    func segSelect(select: Int, index: Int) {
        selectedSegment = select
        segPlanControl.selectedSegmentIndex = index
        
    }
    
    func loadNIB() {
        let Nib1 = UINib(nibName: "CustomPricePlan1", bundle: nil)
        tblViewHome.register(Nib1, forCellReuseIdentifier: "CustomPrice1")
        let Nib2 = UINib(nibName: "CustomPricePlan2", bundle: nil)
        tblViewHome.register(Nib2, forCellReuseIdentifier: "CustomPrice2")
        let Nib3 = UINib(nibName: "CustomPricePlan3", bundle: nil)
        tblViewHome.register(Nib3, forCellReuseIdentifier: "CustomPrice3")
        self.tblViewHome.delegate = self
        self.tblViewHome.dataSource = self
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedSegment == 1 {
            return pricePlan1.count
        } else if selectedSegment == 2 {
            return pricePlan2.count
            
        } else {
            return pricePlan3.count
        }
    }
    
    @IBAction func segControlPlan(_ sender: Any) {
        
        if segPlanControl.selectedSegmentIndex == 0 {
            selectedSegment = 1
            tblViewHome.reloadData()
        } else if segPlanControl.selectedSegmentIndex == 1 {
            selectedSegment = 2
            tblViewHome.reloadData()
            tblViewHome.dataSource = self
            tblViewHome.delegate = self
            
        } else if segPlanControl.selectedSegmentIndex == 2 {
            selectedSegment = 3
            tblViewHome.reloadData()
        } else {
            selectedSegment = 1
            tblViewHome.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellplan1 = tblViewHome.dequeueReusableCell(withIdentifier: "CustomPrice1", for: indexPath) as! CustomPriceCell1
        let cellplan2 = tblViewHome.dequeueReusableCell(withIdentifier: "CustomPrice2", for: indexPath) as! CustomPriceCell2
        let cellplan3 = tblViewHome.dequeueReusableCell(withIdentifier: "CustomPrice3", for: indexPath) as! CustomPriceCell3
        let gradelist = gradeArry[indexPath.row]
        let pricelist1 = pricePlan1 [indexPath.row]
        let pricelist2 = pricePlan2 [indexPath.row]
        let pricelist3 = pricePlan3 [indexPath.row]
        
        switch selectedSegment {
        case 1:
            //cellplan1.txtFGrade1.alighTextViewVertically()
            cellplan1.lblGrade1?.text = gradelist.grade
            cellplan1.lblPrice1Plan1?.text = pricelist1.priceGrade1
            
            
            
        case 2:
            //cellplan2.txtFGrade2.alighTextViewVertically()
            cellplan2.lblGrade2?.text = gradelist.grade
            cellplan2.lblPrice2Plan1?.text = pricelist2.priceGrade1
            cellplan2.lblPrice2Plan2?.text = pricelist2.priceGrade2
            
            
        case 3:
            //cellplan3.txtFGrade3.alighTextViewVertically()
            
            cellplan3.lblGrade3?.text = gradelist.grade
            cellplan3.lblPrice3Plan1?.text = pricelist3.priceGrade1
            cellplan3.lblPrice3Plan2?.text = pricelist3.priceGrade2
            cellplan3.lblPrice3Plan3?.text = pricelist3.priceGrade3
            cellplan3.lblPrice3Plan4?.text = pricelist3.priceGrade4
            
            
        default:
            cellplan1.lblGrade1?.text = gradelist.grade
            cellplan1.lblPrice1Plan1?.text = pricelist1.priceGrade1
            
        }
        if segPlanControl.selectedSegmentIndex == 0{
            return cellplan1
        } else if segPlanControl.selectedSegmentIndex == 1{
            return cellplan2
        } else {
            return cellplan3
        }
    }
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "PricePlan")
    }
    
   
}

