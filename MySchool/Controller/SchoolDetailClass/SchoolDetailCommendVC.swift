//
//  SchoolDetailCommendVC.swift
//  MySchool
//
//  Created by laughinggg on 11/25/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class SchoolDetailCommendVC:UITableViewController,IndicatorInfoProvider{
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Commend")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nibCommend = UINib(nibName: "SchoolDetailCommendNib", bundle: nil)
        tableView.register(nibCommend, forCellReuseIdentifier: "SchCommendCell")
        tableView.rowHeight = 150
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchCommendCell", for: indexPath) as! SchoolDetailCommendClass
        cell.imgProfile?.image = UIImage(named: "image.jpeg")
        cell.lblName?.text = "Visa"
        cell.lblTitle?.text = "Awesome!"
        cell.lblTimeAgo?.text = "5 Minture Ago"
        cell.txtviewCommend?.text = "It is a very a nice school with affordable price.It is a very a nice school with affordable price.It is a very a nice school with affordable price.It is a very a nice school with affordable price."
        
        return cell
    }
    
}
