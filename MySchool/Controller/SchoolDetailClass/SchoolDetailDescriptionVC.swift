//
//  SchoolDetailDescriptionVC.swift
//  MySchool
//
//  Created by laughinggg on 11/25/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class SchoolDetailDescriptionVC: UITableViewController,IndicatorInfoProvider {
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Description")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        //tableView.rowHeight = 160
        let nibDescription = UINib(nibName: "SchoolDetailDescriptionNib", bundle: nil)
        tableView.register(nibDescription, forCellReuseIdentifier: "SchDescriptionCell")
        
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchDescriptionCell", for: indexPath) as! SchoolDetailDescriptionClass
        cell.lblTitle?.text = "Hhhhhi"
        cell.txtviewContent?.text = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda."
        return cell
    }
    
    
    
    
    
}
