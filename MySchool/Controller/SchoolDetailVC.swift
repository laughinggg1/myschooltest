//
//  ParentPriceVC.swift
//  MySchool
//
//  Created by laughinggg on 11/14/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class SchoolDetailVC: ButtonBarPagerTabStripViewController{
    @IBOutlet weak var imgAddOne:UIImageView!
    @IBOutlet weak var colorGradientview:UIView!
    @IBOutlet weak var colorGradientSchoolNameView: UIView!
    @IBOutlet weak var SchNameDetail: UILabel!
    @IBOutlet weak var SchTypeDetail: UILabel!
    @IBOutlet weak var viewStarDetail: UIView!
    
    var schoolStructs = [schoolStruct]()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationTransparaent()
        navigationController?.navigationBar.backgroundColor = .clear
        UINavigationBar.appearance().backgroundColor = UIColor.middlePink
        colorGradient(gradientView: colorGradientview)
        
    }
    override func viewDidLoad() {
        ButtonBarSetting()
        super.viewDidLoad()
        getSchool()
        //navigationTransparaent()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.navigationTransparaent()
        UINavigationBar.appearance().backgroundColor = UIColor.middlePink
        
    }
    
    func ButtonBarSetting() {
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.buttonBarItemFont = UIFont(name: "Helvetica", size: 16.0)!
        settings.style.buttonBarItemTitleColor = .gray
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailableWidth = false
        settings.style.buttonBarLeftContentInset = 15
        settings.style.buttonBarRightContentInset = 15
        settings.style.selectedBarHeight = 1.0
        // settings.style.selectedBarVerticalAlignment = .to
        settings.style.selectedBarBackgroundColor = .orange
        settings.style.buttonBarHeight = 20.0
        // Changing item text color on swipe
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .gray
            newCell?.label.textColor = .orange
        }
    }
    
    func colorGradient(gradientView: UIView){
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        gradientLayer.frame.size = self.colorGradientview.frame.size
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0)
        gradientLayer.colors = [UIColor.white.cgColor, UIColor(red: 201/255, green: 123/255, blue: 219/255, alpha: 1).cgColor]
        gradientView.layer.insertSublayer(gradientLayer, at: 0)
        //gradientView.layer.addSublayer(gradientLayer)
        gradientView.addSubview(imgAddOne)
        
    }
        override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
            let child1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "schooldetaildes")
            let child2 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "schooldetailpriceplansty")
            let child3 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "schooldetailcommendsty")
            let child4 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "schooldetailcontactussty")
            
            return [child1, child2, child3,child4]
        }
    
    func getSchool(){
        guard let url = URL(string: JsonUrl.getSchool(schoolid: "1")) else {return}
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            guard let data = data else {return}
            do {
                let jsondecode = JSONDecoder()
                let schooldata = try jsondecode.decode([schoolStruct].self, from: data )
                self.schoolStructs = schooldata
                DispatchQueue.main.async {
                    self.SchNameDetail?.text = self.schoolStructs[0].name
                    self.SchTypeDetail?.text = self.schoolStructs[0].type
                    guard let logourl = URL(string: JsonUrl.logoUrl(imgName: schooldata[0].logo)) else {return}
                    self.imgAddOne?.sd_setImage(with: logourl, placeholderImage: UIImage(named: "Placeholder.png"))
                }
            }catch{
                Utility.showAlertMessage(title: "Cannot get school information", message: "Unable to get the data from server, Please try again later", inviewcontroller: self)
            }
        }.resume()
    }
    
    @IBAction func barBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

















// Gradient function for the school logo in school detail (not use currently)
//    func colorGradientSchool(gradientView: UIView){
//        let gradientLayer: CAGradientLayer = CAGradientLayer()
//        gradientLayer.frame.size = self.colorGradientview.frame.size
//        gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.5)
//        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0)
//        gradientLayer.colors = [UIColor.white.cgColor, UIColor(red: 201/255, green: 123/255, blue: 219/255, alpha: 1).cgColor]
//        gradientView.layer.insertSublayer(gradientLayer, at: 0)
//        gradientView.layer.addSublayer(gradientLayer)
//        //gradientView.addSubview(subview)
//
//    }
// Some stuff note in here
//buttonBarView.isScrollEnabled = true
//navigationController?.navigationBar.isHidden = true
//navigationController?.navigationBar.isTranslucent = true
//settings.style.buttonBarItemsShouldFillAvailableWidth = true


