//
//  AppDelegate.swift
//  MySchool
//
//  Created by laughinggg on 10/23/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //FirebaseConfiguration.shared.setLoggerLevel(.min)
        GMSServices.provideAPIKey("AIzaSyCe-7anBsZYq4I-MYfE6-utRRbnZUZwF4Q")
        //UINavigationBar.appearance().backgroundColor = UIColor.middlePink
        
        let navBarApperance = UINavigationBar.appearance()
        navBarApperance.backgroundColor = UIColor.middlePink
        navBarApperance.tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 16) as Any,NSAttributedString.Key.foregroundColor:UIColor.textnavColor]
        FirebaseApp.configure()
        
        FBSDKApplicationDelegate.sharedInstance()?.application(application,didFinishLaunchingWithOptions: launchOptions)
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let appId: String = FBSDKSettings.appID()
        if url.scheme != nil && url.scheme!.hasPrefix("fb\(appId)") && url.host == "authorize" {
            let sourceapplicationkey = options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: sourceapplicationkey, annotation: nil)
        }
        return false
    }
    
    
}

