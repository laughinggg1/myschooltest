//
//  Utility.swift
//  MySchool
//
//  Created by laughinggg on 11/29/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import Foundation
import UIKit

struct Utility {
    static func showAlertMessage(title: String, message: String, inviewcontroller vc:UIViewController){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okButton)
        vc.present(alertController, animated: true, completion: nil)
    }
    
}
