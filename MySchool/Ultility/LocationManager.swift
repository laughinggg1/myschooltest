//
//  LocationManager.swift
//  MySchool
//
//  Created by laughinggg on 11/29/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import Foundation
import GoogleMaps

extension UIViewController{
    
    func requestWhenInUseLocation() {
        let locationManager = CLLocationManager()
        locationManager.requestWhenInUseAuthorization()
    }
   
    
}
