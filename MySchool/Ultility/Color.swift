//
//  Color.swift
//  MySchool
//
//  Created by laughinggg on 11/28/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit.UIColor

extension UIColor {
    static var purpleColor:UIColor {
        return UIColor(red: 144/255, green: 19/255, blue: 254/255, alpha: 0.07)
    }
    static var purpleMoreThick:UIColor {
        return UIColor(red: 144/255, green: 19/255, blue: 254/255, alpha: 0.50)
    }
    
    static var darkPurple:UIColor{
        return UIColor(red: 245/255, green: 239/255, blue: 249/255, alpha: 1)
    }
    static var middlePink: UIColor{
        return UIColor(red: 210/255, green: 151/255, blue: 196/255, alpha: 1)
    }
    static var textnavColor: UIColor{
        return UIColor(red: 87/255, green: 96/255, blue: 102/255, alpha: 1)
    }
}
