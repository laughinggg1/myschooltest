//
//  File.swift
//  MySchool
//
//  Created by laughinggg on 10/25/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit

extension UITextView{
    
    func alighTextViewVertically() {
        var txtViewVertically = (self.bounds.size.height - self.contentSize.height * self.zoomScale) / 2
        txtViewVertically = txtViewVertically < 0.0 ? 0.0 :txtViewVertically
        self.contentInset.top = txtViewVertically
    }
    
    
}
