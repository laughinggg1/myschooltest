//
//  NavigationTransparents.swift
//  MySchool
//
//  Created by laughinggg on 11/28/18.
//  Copyright © 2018 laughinggg. All rights reserved.
//

import UIKit

extension UIViewController{
    func navigationTransparaent() {
        guard let nav = navigationController else { return }
        nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nav.navigationBar.shadowImage = UIImage()
        nav.navigationBar.isTranslucent = true
        nav.view.backgroundColor = .clear
        
    }
}
